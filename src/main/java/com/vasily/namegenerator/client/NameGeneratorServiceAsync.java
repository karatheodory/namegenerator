package com.vasily.namegenerator.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * Async version of NameGeneratorService.
 * Created by Vasily Loginov on 16.04.14.
 */
public interface NameGeneratorServiceAsync {
    void regenerateNames(AsyncCallback<Void> callback);
}
