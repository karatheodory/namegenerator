package com.vasily.namegenerator.client;

import com.google.gwt.cell.client.TextCell;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.vasily.namegenerator.shared.FieldVerifier;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.vasily.namegenerator.shared.NameInfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class NameGenerator implements EntryPoint {
    /**
     * The message displayed to the user when the server cannot be reached or
     * returns an error.
     */
    private static final String SERVER_ERROR = "An error occurred while "
            + "attempting to contact the server. Please check your network "
            + "connection and try again.";

    /**
     * Create a remote service proxy to talk to the server-side Greeting service.
     */
    private final NameGeneratorServiceAsync nameGeneratorService = GWT.create(NameGeneratorService.class);

    /**
     * This is the entry point method.
     */
    public void onModuleLoad() {
        // Label to display communication errors.
        final Label infoLabel = new Label();
        RootPanel.get("infoLabelContainer").add(infoLabel);

        // Generation button.
        final Button generateButton = new Button();
        generateButton.setText("Generate");
        generateButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                nameGeneratorService.regenerateNames(new AsyncCallback<Void>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        infoLabel.setText(SERVER_ERROR);
                    }

                    @Override
                    public void onSuccess(Void result) {
                        infoLabel.setText("Names are regenerated successfully!");
                    }
                });
            }
        });

        // Tab panel itself.
        final TabPanel ourTabPanel = new TabPanel();
        final CellTable<NameInfo> table = createCellTable();

        final SimplePager pager = new SimplePager();
        pager.setDisplay(table);
        pager.setPageSize(2);

        final VerticalPanel verticalPanel = new VerticalPanel();
        verticalPanel.add(table);
        verticalPanel.add(pager);

        ourTabPanel.add(generateButton, "Generate");
        ourTabPanel.add(verticalPanel, "View");
        ourTabPanel.selectTab(0);
        RootPanel.get("tabPanelContainer").add(ourTabPanel);
    }

    private CellTable<NameInfo> createCellTable() {
        // http://www.mytechtip.com/2010/11/gwt-celltable-example-using_8168.html
        // http://turbomanage.wordpress.com/2011/03/02/gwt-2-2-asyncdataprovider-celltable-gotcha/
        // http://javaasylum.blogspot.ru/2010/11/gwt-21-request-factory.html
        final CellTable<NameInfo> table = new CellTable<NameInfo>();
        table.setPageSize(3);

        // First name column
        final TextColumn<NameInfo> firstNameColumn = new TextColumn<NameInfo>() {
            @Override
            public String getValue(NameInfo object) {
                return object.getFirstName();
            }
        };
        table.addColumn(firstNameColumn);

        // Second name column
        final TextColumn<NameInfo> familyNameColumn = new TextColumn<NameInfo>() {
            @Override
            public String getValue(NameInfo object) {
                return object.getFamilyName();
            }
        };
        table.addColumn(familyNameColumn);

        // Data provider.
        AsyncDataProvider<NameInfo> provider = new AsyncDataProvider<NameInfo>() {
            @Override
            protected void onRangeChanged(HasData<NameInfo> display) {
                int start = display.getVisibleRange().getStart();
                int end = start + display.getVisibleRange().getLength();
                if (end > NAMES.size())
                    end = NAMES.size();
                List<NameInfo> listToShow = NAMES.subList(start, end);
                updateRowData(start, listToShow);
            }
        };

        provider.addDataDisplay(table);
        provider.updateRowCount(NAMES.size(), true);

        return table;
    }

    private static final List<NameInfo> NAMES = Arrays.asList(
            new NameInfo("Vasily", "Loginov"),
            new NameInfo("Anna", "Loginova"),
            new NameInfo("Viktoria", "Loginova"),
            new NameInfo("Maya", "Loginova")
    );
}
