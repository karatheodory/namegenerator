package com.vasily.namegenerator.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * Name list generation service.
 * Created by Vasily Loginov on 16.04.14.
 */
@RemoteServiceRelativePath("generate")
public interface NameGeneratorService extends RemoteService {
    void regenerateNames();
}
