package com.vasily.namegenerator.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.vasily.namegenerator.client.NameGeneratorService;

/**
 * NameGeneratorService implementation.
 * Created by Vasily Loginov on 16.04.14.
 */
public class NameGeneratorServiceImpl extends RemoteServiceServlet implements NameGeneratorService {
    @Override
    public void regenerateNames() {
        // TODO: here should be generation, do nothing now
    }
}
