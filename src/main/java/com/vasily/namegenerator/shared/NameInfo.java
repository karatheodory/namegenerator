package com.vasily.namegenerator.shared;

import java.io.Serializable;

/**
 * Class describing generated name.
 * Created by Vasily Loginov on 16.04.14.
 */
public class NameInfo implements Serializable {
    private String firstName;
    private String familyName;

    public NameInfo() {}

    public NameInfo(String firstName, String familyName) {
        this.firstName = firstName;
        this.familyName = familyName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}
